package com.cy.jenkinstest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author cy
 * @date 2020/12/14 15:26
 */
@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "hello jenkins";
    }
}
